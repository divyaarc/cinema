class UsersController < ApplicationController
	def signup
       @user=User.new
	end

	
	def signin
		user = User.find_by_username(params[:username])
         if user && user.authenticate(params[:password])
         	session[:user_id] = user.id
         	redirect_to root_url, :notice => "logged in"
         else
         	flash.now.alert = "invalid user name or password"
         	render "signin"
         end

	end
	def check
		user = User.find_by_username(params[:username])
         if user && user.authenticate(params[:password])
         	session[:user_id] = user.id
         	redirect_to root_url, :notice => "logged in"
         else
         	flash.now.alert = "invalid user name or password"
         	render "signin"
         end
	end
	def signout
		session[:user_id]= nil
		redirect_to root_url, :notice => "logged out"
	end


	def create
		@user = User.new(user_params)      
      	if @user.save
      		redirect_to films_users_signin_path, :notice => "You are now a user :)"
      	else
      		render 'signup'
      	end
	end

	
    def index

		@movies = Film.all
	end



def show
	@st=Show.find(:all, :conditions => 'film_id=2')

end

	private
	def user_params
		params.require(:user).permit(:name, :username, :password)
	end

end
