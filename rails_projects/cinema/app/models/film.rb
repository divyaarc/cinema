class Film < ActiveRecord::Base
	has_many :shows, dependent: :destroy
	has_many :bookings, through: :shows
	
end
