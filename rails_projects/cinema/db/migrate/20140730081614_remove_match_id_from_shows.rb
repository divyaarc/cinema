class RemoveMatchIdFromShows < ActiveRecord::Migration
  def change
    remove_column :shows, :match_id, :integer
  end
end
