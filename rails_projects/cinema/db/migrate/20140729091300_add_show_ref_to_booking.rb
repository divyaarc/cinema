class AddShowRefToBooking < ActiveRecord::Migration
  def change
    add_reference :bookings, :show, index: true
  end
end
