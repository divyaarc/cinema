class CreateFilms < ActiveRecord::Migration
  def change
    create_table :films do |t|
      t.string :name
      t.float :rating

      t.timestamps
    end
  end
end
