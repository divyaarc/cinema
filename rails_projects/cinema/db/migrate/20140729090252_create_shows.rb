class CreateShows < ActiveRecord::Migration
  def change
    create_table :shows do |t|
      t.float :timing
      t.integer :vacancies
      t.integer :match_id

      t.timestamps
    end
  end
end
